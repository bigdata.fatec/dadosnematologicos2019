<?php
    
    require 'conecta.php';

    if (!empty($_POST)) {  // Botão Adicionar foi clicado! (Submit) 
        $cod_cliente = $_POST['cod_cliente'];
        header("Location: cadastro_laudo.php?cod_cliente=" . $cod_cliente); 
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
    <title>Inclusão de Laudo</title>
</head>
<body>
    <div class="container">
        <div clas="span10 offset1">
          <div class="card">
            <div class="card-header">
                <h3 class="well" align="center">Inclusão de Laudo</h3>
            </div>
           
            <div class="card-body" align="center">
                <form class="form-horizontal" action="cadastro_laudo_cliente.php" method="post">
                
                <div class="mb-3">
                    <select name="cod_cliente" required>
                    <option value="" disabled selected>== Selecione um cliente ==</option>
                    <?php
                        $pdo = bdNema::conectar();
                        $sql = "SELECT * FROM Clientes ORDER BY nome_cliente";
                        foreach($pdo->query($sql)as $row) {
                            echo ("<option value='" . $row['cod_cliente'] . "'>" . $row['nome_cliente'] . "</option>");
                        }
                        bdNema::desconectar();
                    ?>
                    </select>
                </div>
        		
                <div class="form-actions">
                    <br/>
                    <input class="btn btn-info" type="submit" value="PRÓXIMO">
    	            <a class="btn btn-info" id="btnvoltar" href="homelab.php">CANCELAR</a>
                </div>
            </form>
          </div>
        </div>
        </div>
    </div>
    </div>
</body>
</html>