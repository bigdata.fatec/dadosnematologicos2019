<?php
class bdNema {
    private static $dbHost    = "localhost";
    private static $dbUsuario = "id9109314_nematosystem";
    private static $dbSenha   = "nematoide.123";
    private static $dbNome    = "id9109314_nemadb";
    private static $cont      = null;
    
    public function __construct() {
        die('A função Init nao é permitida!');
    }
    
    public static function conectar() {
        if(null == self::$cont) {
            try {
                self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbNome, self::$dbUsuario, self::$dbSenha); 
            }
            catch(PDOException $exception) {
                die($exception->getMessage());
            }
        }
        return self::$cont;
    }
    
    public static function desconectar() {
        self::$cont = null;
    }
}

?>