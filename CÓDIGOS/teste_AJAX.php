<!doctype html>
<html>
<head>
    <title>How to Auto populate dropdown with jQuery AJAX</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

    <script>
        $(document).ready(function(){

            $("#fazenda").change(function(){
                var cod = $(this).val();

                $.ajax({
                    url: 'ret_talhoes.php',
                    type: 'post',
                    data: {cod_fazenda:cod},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_user").empty();
                        for( var i = 0; i<len; i++){
                            var cod = response[i]['cod'];
                            var nome = response[i]['nome'];

                            $("#sel_user").append("<option value='"+cod+"'>"+nome+"</option>");

                        }
                    }
                });
            });

        });
    </script>
</head>
<body>
    <div>Fazendas:</div>
        <select id="fazenda" required>
        <option value='' disabled selected>== Selecione uma fazenda ==</option>
        <?php
        require 'conecta.php';
            $pdo = bdNema::conectar();
            $sql = 'SELECT * FROM Fazendas';
             
            foreach($pdo->query($sql)as $row) {
                echo ("<option value='" . $row['cod_fazenda'] . "'>" . $row['nome_fazenda'] . "</option>");
            }
            
            bdNema::desconectar();
        ?>
        </select>
        <div class="clear"></div>

        <div>Talhões: </div>
        <select id="sel_user">
            <option value="0">- Select -</option>
        </select>

</body>
</html>