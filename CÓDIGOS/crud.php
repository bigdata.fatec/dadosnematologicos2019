<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>CRUD</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="/bootstrap/bootstrap.min.css" rel="stylesheet">
      <script>
      var $cod = 0;
        $(document).ready(function(){
            $("input:checkbox").on('click', function() {
                var $box = $(this);
                if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
              } else {
                $box.prop("checked", false);
              }
              
              if(this.checked) {
                $cod = this.value;
                } else {
                   $cod = 0; 
                }
            });
            
            $('#btnexcluir').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Sim": function() {
                            $(this).dialog("close");
                            
                            var link = "delete.php?cod_usuario=" + $cod;  
                            window.open(link, "_parent");
                        },
                        Não: function() {
                          $(this).dialog("close");
                        }
                      }
                    });
                }
            });
            
        $('#btninfo').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "read.php?cod_usuario=" + $cod;
                    window.open(link, "_blank");
                }
            });    
            
            $('#btnatualizar').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "update.php?cod_usuario=" + $cod; 
                    window.open(link, "_parent");
                }
            }); 
            
        });
      </script>
    </head>    
    <body>
        <div class="container-fluid">
            <h2>Análise de Dados Nematológicos - <span class="badge badge-secondary">v 1.0.3</span></h2>
        <p>
            
        <div id="dialog-confirm" title="Excluir registro?" style="display:none">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Este registro será excluído permanentemente. Tem certeza, meu filho?</p>
        </div>
        
        <div id="dialog-alert1" title="CRUD" style="display:none">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Selecione um registro!</p>
        </div>

        <a class="btn btn-info" href="index.html">HOME</a>
        <a class="btn btn-info" href="create.php?id=crud">ADICIONAR</a>
        <a class="btn btn-info" href="#" id="btninfo">INFO</a>
        <a class="btn btn-info" href="#" id="btnatualizar">ATUALIZAR</a>
        <a class="btn btn-info" href="#" id="btnexcluir">EXCLUIR</a>
        </p>
            <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Cod.</th>
                            <th scope="col">Usuário</th>
                            <th scope="col">Perfil</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include 'conecta.php';
                        $pdo = bdNema::conectar();
                        $sql = 'SELECT * FROM Usuarios ORDER BY cod_usuario';
                           
                        foreach($pdo->query($sql)as $row) {
                            echo '<tr>';
                            echo '<td>' . '<input type="checkbox" name="chkcod[]" id="chkcod[]" value="' .  $row['cod_usuario'] . '">' . '</td>';
                            
			                echo '<th scope="row">'. $row['cod_usuario'] . '</th>';
			                echo '<td>'. $row['usuario'] . '</td>';
                            
                            if ($row['cod_perfil'] == 1) {
                                echo '<td>'. 'Cliente' . '</td>';
                            } else {
                                echo '<td>'. 'Prestador' . '</td>';
                            }
                            
                            echo '</tr>';
                        }
                       
                        bdNema::desconectar();
                        ?>
                    </tbody>
                </table>
        </div>
    </body>
</html>