<?php
    require 'conecta.php';
   
    $cod_fazenda = $_POST['cod_fazenda'];  
    
    $pdo = bdNema::conectar();
    $sql = "SELECT cod_talhao, nome_talhao FROM Talhoes WHERE cod_fazenda = $cod_fazenda";
    
    $talhoes_array = array();
    
    foreach($pdo->query($sql)as $row) {
        $cod_talhao = $row['cod_talhao'];
        $nome_talhao = $row['nome_talhao'];
        $talhoes_array[] = array("cod" => $cod_talhao, "nome" => $nome_talhao);
    }
 
    // encoding array to json format
    echo json_encode($talhoes_array);
?>

