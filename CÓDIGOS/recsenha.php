<?php
    require 'conecta.php';

    if(!empty($_POST)) {
        $email = $_POST['email'];
        $email = strtolower($email);
        $email = trim($email);
           
        $pdo = bdNema::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Usuarios WHERE email = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($email));
         
        if ($q->rowCount() > 0){
            $data = $q->fetch(PDO::FETCH_ASSOC);
        	$username = $data['username'];
        	$senha    = $data['senha'];
            $nome     = $data['nome'];
           
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
            $from    = "pi.nematoide@gmail.com";
            $to      = $email;
            $subject = "Sua senha para entrar no sistema ADN";
            $message = $senha; 
            $headers = "De:". $from;
            mail($to, $subject, $message, $headers);
        }
        bdNema::desconectar();
        
        //header("Location: index.html");
    }
?>
<!doctype html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Recuperação de senha</title>
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center align-items-center" style="height:100vh">
            <div class="col-4" align="center">
               <p class='lead'>Recuperação de Senha</p>
               
               <p></p><?php echo ("Senha: " . $senha); ?></p>
               <p></p><?php echo ("Nome: " . $nome); ?></p>
               
               
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="recsenha.php">
                            <div class="form-group">
                                <p class="text-center">Digite o email cadastrado. Se o email estiver correto, a sua senha será enviada para ele.</p>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                           <button type="submit" class="btn btn-primary mb-3">ENVIAR</button>
                <button type="button" class="btn btn-primary mb-3" onclick="location.href = 'index.html'">CANCELAR</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>